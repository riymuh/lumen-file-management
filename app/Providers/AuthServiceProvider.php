<?php

namespace App\Providers;

use App\Model\User;
use App\Model\Image;
use App\Model\ImageCategory;
use App\Model\File;
use Illuminate\Support\Facades\Gate;
use App\Policies\ImagePolicy;
use App\Policies\CategoryPolicy;
use App\Policies\FilePolicy;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    protected $policies = [
        'App\Model\Image' => 'App\Policies\ImagePolicy',
        'App\Model\ImageCategory' => 'App\Policies\CategoryPolicy',
        'App\Model\File' => 'App\Policies\FilePolicy'
    ];

    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        Gate::before(function($user, $ability) {
            //var_dump($ability);
            if ($user->hasPermission($ability)) {
                  return true;
            }else{
                $msgs['status'] = 'unauthorization';
                $msgs['msg'] = "you don't have permission to access this route";
                die(json_encode($msgs));
            }
        });
        Gate::policy(Image::class, ImagePolicy::class);
        Gate::policy(ImageCategory::class, CategoryPolicy::class);
        Gate::policy(File::class, FilePolicy::class);

        $this->app['auth']->viaRequest('api', function ($request) {
            // if ($request->input('api_token')) {
            //     return User::where('api_token', $request->input('api_token'))->first();
            // }

            if ($request->header('Authorization')) {
                $explode = explode(' ', $request->header('Authorization'));
                return User::where('api_token', end($explode))->first();
            }
        });
    }
}

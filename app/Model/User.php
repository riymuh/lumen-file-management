<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Auth;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $guarded = [];

    protected $hidden = [
        'password',
    ];

    public function images()
    {
        return $this->hasMany('App\Model\Image');
    }

    public function image_categories()
    {
        return $this->hasMany('App\Model\ImageCategory');
    }

    public function files()
    {
        return $this->hasMany('App\Model\File');
    }

    public function role_access() {
        return $this->belongsTo('App\Model\Role', 'role_id');
    }

    public function hasPermission($permission) {
        // var_dump($this->role_access);
        // die();
        return $this->role_access->permissions()->where('name', $permission)->first() ? true : false;
    }

    public function hasRole($role)
    {
        return Auth::user()->role === $role;
    }

    public function cekStatus()
    {
        return Auth::user()->status;
    }
}

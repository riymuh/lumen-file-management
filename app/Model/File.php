<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $guarded = [];

    protected $appends = ['file_url'];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function getFileUrlAttribute()
    {
        return url('files/user_'.$this->user->id.'/'. $this->file);
    }
}

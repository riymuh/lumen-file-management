<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    ///protected $guarded = [];
    protected $table = 'roles';

    protected $fillable = [
        'name'
    ];

    public function users()
    {
        return $this->hasMany('App\Model\User');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Model\Permission');
    }
}

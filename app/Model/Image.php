<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded = [];

    protected $appends = ['photo_url'];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function image_category()
    {
        return $this->belongsTo('App\Model\ImageCategory', 'category_id');
    }

    public function getPhotoUrlAttribute()
    {
        return url('images/user_'.$this->user->id.'/category_'.$this->image_category->id.'/'. $this->image);
    }

}

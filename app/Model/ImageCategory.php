<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ImageCategory extends Model
{
    protected $guarded = [];
    protected $table = 'image_categories';

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function images()
    {
        return $this->hasMany('App\Model\Image', 'category_id');
    }
}

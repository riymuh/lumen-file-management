<?php

namespace App\Policies;

use App\Model\Image;
use App\Model\User;

class ImagePolicy
{
    public function update_image(User $user, Image $image)
    {
        return $user->id === $image->user_id;
    }
}

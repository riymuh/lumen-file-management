<?php

namespace App\Policies;

use App\Model\ImageCategory;
use App\Model\User;

class CategoryPolicy
{
    public function update_category(User $user, ImageCategory $image)
    {
        return $user->id === $image->user_id;
    }
}

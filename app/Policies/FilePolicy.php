<?php

namespace App\Policies;

use App\Model\File;
use App\Model\User;

class FilePolicy
{
    public function update_file(User $user, File $file)
    {
        return $user->id === $file->user_id;
    }
}

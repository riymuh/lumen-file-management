<?php

namespace App\Transformers;

use App\Model\File;
use League\Fractal\TransformerAbstract;

class FileTransformer extends TransformerAbstract
{
    public function transform(File $file)
    {
        return [
            'id'                => $file->id,
            'file'              => $file->file_url,
            'registered'        => $file->created_at->diffForHumans(),
        ];
    }
}



<?php

namespace App\Transformers;

use App\Model\Image;
use League\Fractal\TransformerAbstract;

class ImageTransformer extends TransformerAbstract
{
    public function transform(Image $image)
    {
        return [
            'id'            => $image->id,
            'image'         => $image->photo_url,
            'registered'    => $image->created_at->diffForHumans(),
        ];
    }
}



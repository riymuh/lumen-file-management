<?php

namespace App\Transformers;

use App\Model\Image;
use App\Model\ImageCategory;
use App\Transformers\ImageTransformer;
use League\Fractal\TransformerAbstract;
use Auth;

class CategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'images',
    ];

    public function transform(ImageCategory $category)
    {

        return [
            'id'            => $category->id,
            'name'         => $category->name,
            'registered'    => $category->created_at->diffForHumans(),
        ];
    }

    public function includeImages(ImageCategory $category)
    {
        $images = $category->images;
        return $this->collection($images, new ImageTransformer);
    }
}



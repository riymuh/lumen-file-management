<?php

namespace App\Transformers;

use App\Model\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(user $user)
    {
        return [
            'id'                => $user->id,
            'name'              => $user->name,
            'email'             => $user->email,
            'phone'             => $user->phone_number,
            'role'              => $user->role,
            'status'            => $user->status,
            'registered'        => $user->created_at->diffForHumans(),
        ];
    }
}



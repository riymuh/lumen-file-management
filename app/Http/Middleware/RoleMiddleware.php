<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    public function handle($request, Closure $next, $role)
    {
        if (!$request->user()->hasRole($role)) {
            return response()->json(['status'=>'Unauthorized']);
        }
        //return $request->user()->cekStatus();
        if($request->user()->cekStatus() == 'blocked'){
            return response()->json(['status'=>'Your has been blocked by admin, please contact admin for confirmation']);
        }

        return $next($request);
    }
}

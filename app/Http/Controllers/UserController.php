<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\User;
use App\Transformers\UserTransformer;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        //
    }

    public function getProfile()
    {
        $user = User::find(Auth::user()->id);
        return $user->role_access;
        $data = fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->toArray();

        return response()->json(["status" => "success", "data" => $data]);
    }

    public function changeToken()
    {
        $user = User::find(Auth::user()->id);

        $token = Str::random(40);
        $user->update(['api_token' => $token]);

        $data = fractal()
                ->item($user)
                ->transformWith(new UserTransformer)
                ->addMeta([
                    "token" => $user->api_token
                ])
                ->toArray();

        return response()->json(["status" => "success", "data" => $data]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Model\User;
use App\Transformers\UserTransformer;
use Auth;

class AdminUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getUsers()
    {
        if($this->authorize('admin.users') || $this->authorize('admin.users.get_users'))
        {
            $users = User::where('id', '!=', Auth::user()->id)
                            ->orderBy('created_at', 'DESC')
                            ->get();

            $data = fractal()
                ->collection($users)
                ->transformWith(new UserTransformer)
                ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

    public function getUser($id)
    {
        if($this->authorize('admin.users') || $this->authorize('admin.users.get_user'))
        {
            $user = User::where('id', '!=', Auth::user()->id)
                            ->where('id', $id)
                            ->first();

            $data = fractal()
                    ->item($user)
                    ->transformWith(new UserTransformer)
                    ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

    public function storeUser(Request $request)
    {
        if($this->authorize('admin.users') || $this->authorize('admin.users.store'))
        {
            $this->validate($request, [
                'name' => 'required|string',
                'address' => 'required|string',
                'email' => 'required|string',
                'password' => 'required|string',
                'phone_number' => 'required|string',
            ]);

            $token = Str::random(40);

            User::create([
                'name' => $request->name,
                'address' => $request->address,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'phone_number' => $request->phone_number,
                'api_token' => $token,
            ]);

            return response()->json(['status' => 'success']);
        }
    }


    public function updateUser(Request $request, $id)
    {
        if($this->authorize('admin.users') || $this->authorize('admin.users.update'))
        {
            $user = User::where('id', '!=', Auth::user()->id)
                            ->where('id', $id)
                            ->first();

            $user->update([
                'status' => $request->status
            ]);

            return response()->json(["status" => "success"]);
        }
    }

    public function deleteUser($id)
    {
        if($this->authorize('admin.users') || $this->authorize('admin.users.delete'))
        {
            $user = User::where('id', '!=', Auth::user()->id)
                            ->where('id', $id)
                            ->first();

            $user->delete();

            return response()->json(["status" => "success"]);
        }
    }
}

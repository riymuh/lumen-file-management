<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Transformers\CategoryTransformer;
use App\Model\Image;
use App\Model\ImageCategory;
use App\Policies\CategoryPolicy;
use Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        //
    }

    public function getCategories()
    {
        $this->authorize('user.categories.get_categories');

        $categories = ImageCategory::where('user_id', Auth::user()->id)->get();

        $data = fractal()
            ->collection($categories)
            ->transformWith(new CategoryTransformer)
            ->toArray();

        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function getCategory($id)
    {
        $this->authorize('user.categories.get_category');

        $category = ImageCategory::find($id);
        if (Gate::allows('update_category', $category)) {
            $data = fractal()
                    ->item($category)
                    ->transformWith(new CategoryTransformer)
                    ->includeImages()
                    ->toArray();

            return response()->json(['status' => 'success', 'data' => $data]);
        }else{
            return response()->json(['status'=> 'Unauthorization']);
        }

    }

    public function storeCategory(Request $request)
    {
        $this->authorize('user.categories.store');

        $this->validate($request, [
            'name' => 'required|string',
        ]);

        ImageCategory::create([
            'name' => $request->name,
            'user_id' => Auth::user()->id
        ]);

        return response()->json(['status' => 'success']);
    }

    public function updateCategory(Request $request, $id)
    {
        $this->authorize('user.categories.update');

        $this->validate($request, [
            'name' => 'required|string',
        ]);

        $category = ImageCategory::find($id);

        if (Gate::allows('update_category', $category)) {
            $category->update([
                'name' => $request->name,
            ]);
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status'=> 'Unauthorization']);
        }
    }

    public function deleteCategory($id)
    {
        $this->authorize('user.categories.delete');

        $category = ImageCategory::find($id);

        //delete all image
        //GATE
        if (Gate::allows('update_category', $category)) {
            $category->delete();
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status'=> 'Unauthorization']);
        }


    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Model\User;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'email.exists' => 'You are not registered User. Please register.',
            'password' => 'required|string|min:6'
        ]);

        $user = User::where('email', $request->email)->first();
        //return $user->images;
        if($user && Hash::check($request->password, $user->password)){
            // $token = Str::random(40);
            // $user->update(['api_token' => $token]);
            return response()->json(['status'=>'success', 'data'=>$user->api_token]);
        }
        return response()->json(['status'=>'error']);
    }
}

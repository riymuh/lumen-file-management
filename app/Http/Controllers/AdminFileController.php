<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\User;
use App\Transformers\UserTransformer;
use App\Model\File;
use App\Transformers\FileTransformer;
use Auth;

class AdminFileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getFiles($id)
    {
        if($this->authorize('admin.files') || $this->authorize('admin.files.get_files'))
        {
            $files = File::where('user_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();

            $data = fractal()
            ->collection($files)
            ->transformWith(new FileTransformer)
            ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

    public function getFile($user_id, $file_id)
    {
        if($this->authorize('admin.files') || $this->authorize('admin.files.get_file'))
        {
            $file = File::where('user_id', $user_id)
                        ->where('id', $file_id)
                        ->first();

            $data = fractal()
                ->item($file)
                ->transformWith(new FileTransformer)
                ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

    public function deleteFile($user_id, $file_id)
    {
        if($this->authorize('admin.files') || $this->authorize('admin.files.delete'))
        {
            $file = File::where('user_id', $user_id)
                        ->where('id', $file_id)
                        ->first();

            unlink(base_path('public/files/user_'.$user_id.'/'.$file->file));
            $file->delete();

            return response()->json(["status" => "success"]);
        }
    }

    //
}

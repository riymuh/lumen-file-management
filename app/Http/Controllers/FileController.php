<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate;
use App\Transformers\FileTransformer;
use App\Policies\FilePolicy;
use App\Model\User;
use App\Model\File;
use Auth;
use App\Helpers\Setting;

class FileController extends Controller
{
    protected $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }

    public function storeFile(Request $request)
    {
        $this->authorize('user.files.store');

        $this->validate($request, [
            'file' => 'required|mimes:pdf,zip,rar,doc,docx,xls,xlsx | max:'.$this->setting::size_of_file()->max_size_file,
        ]);

        $extenstionFile = $request->file('file')->clientExtension();
        $filename = null;
        if($request->hasFile('file')){
            $filename = Str::random(40).'.'.$extenstionFile;
            $file = $request->file('file');
            $file->move(base_path('public/files/user_'.Auth::user()->id.'/'), $filename);
        }

        File::create([
            'file' => $filename,
            'user_id' => Auth::user()->id,
        ]);

        return response()->json(['status' => 'success']);
    }

    public function getFiles()
    {
        $this->authorize('user.files.get_files');

        $files = File::where('user_id', Auth::user()->id)
                        ->orderBy('created_at', 'DESC')
                        ->get();

        $data = fractal()
            ->collection($files)
            ->transformWith(new FileTransformer)
            ->toArray();

        return response()->json(["status" => "success", "data" => $data]);
    }

    public function getFile($id)
    {
        $this->authorize('user.files.get_file');

        $file = File::find($id);
        if (Gate::allows('update_file', $file)) {
            $data = fractal()
                ->item($file)
                ->transformWith(new FileTransformer)
                ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }else{
            return response()->json(['status'=> 'Unauthorization']);
        }
    }

    public function deleteFile($id)
    {
        $this->authorize('user.files.delete');

        $file = File::find($id);
        if (Gate::allows('update_file', $file)) {
            unlink(base_path('public/files/user_'.Auth::user()->id.'/'.$file->file));
            $file->delete();
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status'=> 'Unauthorization']);
        }

    }
}

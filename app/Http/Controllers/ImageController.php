<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Transformers\ImageTransformer;
use Illuminate\Support\Facades\Gate;
use App\Model\User;
use App\Model\Image;
use App\Model\ImageCategory;
use App\Policies\ImagePolicy;
use Auth;
use App\Helpers\Setting;

class ImageController extends Controller
{
    protected $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }

    public function storeImage(Request $request)
    {
        $this->authorize('user.images.store');

        $this->validate($request, [
            'image' => 'required|mimes:jpg,jpeg,png | max:'.$this->setting::size_of_file()->max_size_image,
            'category_id' => 'required',
        ]);

        $image_category = ImageCategory::find($request->category_id);
        $filename = null;
        if($request->hasFile('image')){
            $filename = Str::random(40).'.jpg';
            $file = $request->file('image');
            $file->move(base_path('public/images/user_'.Auth::user()->id.'/category_'.$image_category->id.'/'), $filename);
        }

        Image::create([
            'image' => $filename,
            'user_id' => Auth::user()->id,
            'category_id' => $image_category->id
        ]);

        return response()->json(['status' => 'success']);
    }

    public function getImages()
    {
        $this->authorize('user.images.get_images');

        $images = Image::where('user_id', Auth::user()->id)
                        ->orderBy('id', 'DESC')
                        ->get();

        $data = fractal()
            ->collection($images)
            ->transformWith(new ImageTransformer)
            ->toArray();

        return response()->json(["status" => "success", "data" => $data]);
    }

    public function getImage($id)
    {
        $this->authorize('user.images.get_image');

        $image = Image::find($id);
        if (Gate::allows('update_image', $image)) {
            $data = fractal()
                ->item($image)
                ->transformWith(new ImageTransformer)
                ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }else{
            return response()->json(['status'=> 'Unauthorization']);
        }
    }

    public function deleteImage($id)
    {
        $this->authorize('user.images.delete');

        $image = Image::find($id);

        if (Gate::allows('update_image', $image)) {
            unlink(base_path('public/images/user_'.Auth::user()->id.'/category_'.$image->image_category->id.'/'.$image->image));
            $image->delete();
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status'=> 'Unauthorization']);
        }
    }
}

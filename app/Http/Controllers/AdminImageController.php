<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\User;
use App\Transformers\UserTransformer;
use App\Model\Image;
use App\Transformers\ImageTransformer;
use App\Model\ImageCategory;
use App\Transformers\CategoryTransformer;
use Auth;

class AdminImageController extends Controller
{
    public function __construct()
    {
        //
    }

    public function getCategories($id)
    {
        if($this->authorize('admin.images') || $this->authorize('admin.images.get_categories'))
        {
            $categories = ImageCategory::where('id', $id)
                        ->orderBy('created_at', 'DESC')
                        ->get();

            $data = fractal()
                    ->collection($categories)
                    ->transformWith(new CategoryTransformer)
                    ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

    public function getImages($user_id, $category_id)
    {
        if($this->authorize('admin.images') || $this->authorize('admin.images.get_images'))
        {
            $images = Image::where('category_id', $category_id)
                            ->where('user_id', $user_id)
                            ->orderBy('created_at', 'DESC')
                            ->get();

            $data = fractal()
                    ->collection($images)
                    ->transformWith(new ImageTransformer)
                    ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

    public function getImage($user_id, $category_id, $image_id)
    {
        if($this->authorize('admin.images') ||  $this->authorize('admin.images.get_image'))
        {
            $image = Image::where('category_id', $category_id)
                        ->where('user_id', $user_id)
                        ->where('id', $image_id)
                        ->first();

            $data = fractal()
                    ->item($image)
                    ->transformWith(new ImageTransformer)
                    ->toArray();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

    public function deleteImage($user_id, $category_id, $image_id)
    {
        if($this->authorize('admin.images') || $this->authorize('admin.images.delete'))
        {
            $image = Image::where('category_id', $category_id)
                        ->where('user_id', $user_id)
                        ->where('id', $image_id)
                        ->first();

            unlink(base_path('public/images/user_'.$user_id.'/category_'.$category_id.'/'.$image->image));
            $image->delete();

            return response()->json(["status" => "success", "data" => $data]);
        }
    }

}

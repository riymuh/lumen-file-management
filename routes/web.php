<?php

$router->post('/login', 'LoginController@login');

$router->group(['middleware' => ['auth']], function() use($router){
    //profile
    $router->get('/profile', 'UserController@getProfile');
    $router->post('/change-token', 'UserController@changeToken');
});

$router->group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'admin'], function() use($router){
    //users
    $router->get('/users', 'AdminUserController@getUsers');
    $router->post('/users/store', 'AdminUserController@storeUser');
    $router->get('/users/{id}', 'AdminUserController@getUser');
    $router->put('/users/{id}/update', 'AdminUserController@updateUser');
    $router->delete('/users/{id}/delete', 'AdminUserController@deleteUser');

    //categories
    $router->get('/users/{id}/categories', 'AdminImageController@getCategories');
    $router->get('/users/{user_id}/categories/{category_id}/images', 'AdminImageController@getImages');
    $router->get('/users/{user_id}/categories/{category_id}/images/{image_id}', 'AdminImageController@getImage');
    $router->delete('/users/{user_id}/categories/{category_id}/images/{image_id}/delete', 'AdminImageController@deleteImage');

    //files
    $router->get('/users/{id}/files', 'AdminFileController@getFiles');
    $router->get('/users/{user_id}/files/{file_id}', 'AdminFileController@getFile');
    $router->delete('/users/{user_id}/files/{file_id}/delete', 'AdminFileController@deleteFile');
});

$router->group(['middleware' => ['auth', 'role:user'], 'prefix' => 'user'], function() use($router){
    //images
    $router->get('/images/get-all', 'ImageController@getImages');
    $router->post('/images/store', 'ImageController@storeImage');
    $router->get('/images/{id}', 'ImageController@getImage');
    $router->delete('/images/{id}/delete', 'ImageController@deleteImage');

    //image categories
    $router->get('/categories', 'CategoryController@getCategories');
    $router->get('/categories/{id}', 'CategoryController@getCategory');
    $router->put('/categories/{id}/update', 'CategoryController@updateCategory');
    $router->delete('/categories/{id}/delete', 'CategoryController@deleteCategory');
    $router->post('/categories/store', 'CategoryController@storeCategory');

    //files
    $router->get('/files/get-all', 'FileController@getFiles');
    $router->post('/files/store', 'FileController@storeFile');
    $router->get('/files/{id}', 'FileController@getFile');
    $router->delete('/files/{id}/delete', 'FileController@deleteFile');
});


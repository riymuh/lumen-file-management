-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 04 Jan 2021 pada 10.08
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lumen_file_management`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `files`
--

INSERT INTO `files` (`id`, `user_id`, `file`, `created_at`, `updated_at`) VALUES
(5, 1, 'hrA8Ckti9sjcsoETcb3NI80oECCsQH9eZW069exM.docx', '2020-12-03 03:58:29', '2020-12-03 03:58:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `general_setting`
--

CREATE TABLE `general_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `max_size_image` bigint(20) NOT NULL,
  `max_size_file` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `general_setting`
--

INSERT INTO `general_setting` (`id`, `max_size_image`, `max_size_file`, `created_at`, `updated_at`) VALUES
(1, 100, 100, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `images`
--

INSERT INTO `images` (`id`, `category_id`, `user_id`, `image`, `created_at`, `updated_at`) VALUES
(4, 3, 2, 'n1GnuTwrhpLg1E5K21z27CfaHAtqmXbofW9valQJ.jpg', '2020-12-02 04:12:20', '2020-12-02 04:12:20'),
(5, 1, 1, 'rToKp58WZP3S11YBp945tMoKjkm3YPxrjaOqgkBj.jpg', '2020-12-02 04:12:25', '2020-12-02 04:12:25'),
(6, 1, 1, 'mmom7BtwS3GSEC5ZYevmwNLbbwJTrB5cmF0yV3eB.jpg', '2020-12-02 06:52:55', '2020-12-02 06:52:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `image_categories`
--

CREATE TABLE `image_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `image_categories`
--

INSERT INTO `image_categories` (`id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'php', '2020-12-01 02:32:23', NULL),
(2, 1, 'vue', '2020-12-01 02:55:08', '2020-12-02 07:36:19'),
(3, 2, 'node js', '2020-12-01 06:39:53', NULL),
(4, 1, 'nokia', '2020-12-02 07:15:07', '2020-12-02 07:15:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_11_30_074458_create_users_table', 1),
(2, '2020_12_01_023608_create_images_table', 2),
(3, '2020_12_02_022619_create_image_categories_table', 3),
(4, '2020_12_02_023926_add_category_id_to_images_table', 4),
(5, '2020_12_03_023108_create_files_table', 5),
(6, '2020_12_04_021322_create_general_setting_table', 6),
(7, '2020_12_04_031232_create_roles_table', 7),
(8, '2020_12_04_031244_create_permissions_table', 7),
(9, '2020_12_04_031303_create_permission_role_table', 7),
(10, '2020_12_04_031517_add_role_id_to_users_table', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin.files', NULL, NULL),
(2, 'admin.files.get_files', NULL, NULL),
(3, 'admin.files.get_file', NULL, NULL),
(4, 'admin.files.delete', NULL, NULL),
(5, 'admin.images', NULL, NULL),
(6, 'admin.images.get_categories', NULL, NULL),
(7, 'admin.images.get_images', NULL, NULL),
(8, 'admin.images.get_image', NULL, NULL),
(9, 'admin.images.delete', NULL, NULL),
(10, 'admin.users', NULL, NULL),
(11, 'admin.users.get_users', NULL, NULL),
(12, 'admin.users.get_user', NULL, NULL),
(13, 'admin.users.store', NULL, NULL),
(14, 'admin.users.update', NULL, NULL),
(15, 'admin.users.delete', NULL, NULL),
(16, 'user.files', NULL, NULL),
(17, 'user.files.store', NULL, NULL),
(18, 'user.files.get_files', NULL, NULL),
(19, 'user.files.get_file', NULL, NULL),
(20, 'user.categories.get_categories', NULL, NULL),
(21, 'user.categories.get_category', NULL, NULL),
(22, 'user.categories.store', NULL, NULL),
(23, 'user.categories.update', NULL, NULL),
(24, 'user.categories.delete', NULL, NULL),
(25, 'user.categories', NULL, NULL),
(26, 'user.images', NULL, NULL),
(27, 'user.images.get_images', NULL, NULL),
(28, 'user.images.get_image', NULL, NULL),
(29, 'user.images.store', NULL, NULL),
(30, 'user.images.update', NULL, NULL),
(31, 'user.images.delete', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 5, NULL, NULL),
(3, 1, 10, NULL, NULL),
(4, 2, 1, NULL, NULL),
(5, 2, 2, NULL, NULL),
(6, 2, 3, NULL, NULL),
(7, 2, 4, NULL, NULL),
(8, 3, 5, NULL, NULL),
(9, 3, 6, NULL, NULL),
(10, 3, 7, NULL, NULL),
(11, 3, 8, NULL, NULL),
(12, 3, 9, NULL, NULL),
(13, 4, 10, NULL, NULL),
(14, 4, 11, NULL, NULL),
(15, 4, 12, NULL, NULL),
(16, 4, 13, NULL, NULL),
(17, 4, 14, NULL, NULL),
(18, 4, 15, NULL, NULL),
(19, 5, 16, NULL, NULL),
(20, 5, 17, NULL, NULL),
(21, 5, 18, NULL, NULL),
(22, 5, 19, NULL, NULL),
(23, 6, 20, NULL, NULL),
(24, 6, 21, NULL, NULL),
(25, 6, 22, NULL, NULL),
(26, 6, 23, NULL, NULL),
(27, 6, 24, NULL, NULL),
(28, 6, 25, NULL, NULL),
(29, 7, 26, NULL, NULL),
(30, 7, 27, NULL, NULL),
(31, 7, 28, NULL, NULL),
(32, 7, 29, NULL, NULL),
(33, 7, 30, NULL, NULL),
(34, 7, 31, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin master', NULL, NULL),
(2, 'admin files', NULL, NULL),
(3, 'admin images', NULL, NULL),
(4, 'admin users', NULL, NULL),
(5, 'user files', NULL, NULL),
(6, 'user categories', NULL, NULL),
(7, 'user images', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `status` enum('allowed','blocked') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'allowed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `email`, `password`, `phone_number`, `api_token`, `role`, `status`, `created_at`, `updated_at`, `role_id`) VALUES
(1, 'admin', 'admin', 'admin@mail.com', '$2y$10$7w064cJSfoZYpITP/Vzf6OqgKRQnoDmjxykL3YQMeQYNQQ46PXjF.', '0899999999', '1lln7HylYr2Bjj1aGt3pwSwiko3i6moReVThNHka', 'admin', 'allowed', '2020-11-30 08:14:33', '2020-12-03 07:54:54', 1),
(2, 'admin file', 'admin file', 'adminfile@mail.com', '$2y$10$7w064cJSfoZYpITP/Vzf6OqgKRQnoDmjxykL3YQMeQYNQQ46PXjF.', '08999999', '5aHSH7FTbfi8YfwoWVeXO9B1EduDZqqkTQj93z', 'admin', 'allowed', '2020-12-01 08:14:42', NULL, 2),
(5, 'riyadh', 'jalan jalaln', 'mriyadh103@gmail.com', '$2y$10$ZFSUtE2bOKL3TLJDe1wggugPFm2UsA0q.YwOBS/Xn5KWmLm7D/9Jm', '086615212', '9pmOGCZUqT1cv5DBy4G9gLyDZSJuJGB5I1arwyFY', 'user', 'allowed', '2020-12-03 06:46:41', '2020-12-03 06:47:11', 6);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `general_setting`
--
ALTER TABLE `general_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `image_categories`
--
ALTER TABLE `image_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_categories_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `general_setting`
--
ALTER TABLE `general_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `image_categories`
--
ALTER TABLE `image_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `image_categories`
--
ALTER TABLE `image_categories`
  ADD CONSTRAINT `image_categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
